package dev.moderocky.rainbow.proxy;

import com.moderocky.mask.template.BungeePlugin;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.internal.CoreConstant;

public class Rainbungee extends BungeePlugin implements CoreConstant {

    public Rainbow rainbow;

    @Override
    public void startup() {
        rainbow = new Rainbow(this);
    }

    @Override
    public void disable() {
        rainbow.shutdown();
        rainbow = null;
    }

}
