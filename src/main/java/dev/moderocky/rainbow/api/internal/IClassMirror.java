package dev.moderocky.rainbow.api.internal;

public interface IClassMirror<C> {

    void apply(C object);

    void reflect(C object);

}
