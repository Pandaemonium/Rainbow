package dev.moderocky.rainbow.api.internal;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.storage.CacheDAO;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface JSerialiser {

    default @NotNull Collection<String> getAsStrings(JsonArray array) {
        List<String> strings = new ArrayList<>();
        for (JsonElement element : array) {
            try {
                strings.add(element.getAsString());
            } catch (Throwable ignore) {
            }
        }
        return strings;
    }

    default @NotNull byte[] getAsByteArray(JsonArray array) {
        List<Byte> list = new ArrayList<>();
        for (JsonElement element : array) {
            try {
                list.add(element.getAsByte());
            } catch (Throwable ignore) {
            }
        }
        byte[] bytes = new byte[list.size()];
        int i = 0;
        for (Byte aByte : list) {
            bytes[i] = aByte;
        }
        return bytes;
    }

    default @NotNull Map<String, Boolean> getAsBooleanMap(JsonArray array, final Map<String, Boolean> map) {
        for (JsonElement element : array) {
            try {
                JsonObject object = (JsonObject) element;
                object.entrySet().forEach(entry -> {
                    map.put(entry.getKey(), entry.getValue().getAsBoolean());
                });
            } catch (Throwable ignore) {
            }
        }
        return map;
    }

    default JsonArray toArray(Collection<Group> list) {
        JsonArray array = new JsonArray();
        for (Group group : list) {
            array.add(group.getID());
        }
        return array;
    }

    default JsonArray toArray(Map<String, Boolean> map) {
        JsonArray array = new JsonArray();
        for (Map.Entry<String, Boolean> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty(entry.getKey(), entry.getValue());
            array.add(object);
        }
        return array;
    }

    default List<Group> toGroups(Collection<String> list) {
        List<Group> groups = new ArrayList<>();
        CacheDAO dao = Rainbow.DAO.getCache();
        list.forEach(string -> groups.add(dao.getProvisionalGroup(string)));
        return groups;
    }

}
