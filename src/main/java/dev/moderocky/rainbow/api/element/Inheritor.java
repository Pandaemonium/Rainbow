package dev.moderocky.rainbow.api.element;

import java.util.List;

public interface Inheritor<T extends Element> {

    boolean hasAncestors();

    List<T> getAncestors();

    boolean hasDescendants();

    List<T> getDescendants();

}
