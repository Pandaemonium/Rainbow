package dev.moderocky.rainbow.api.element;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.storage.CacheEntry;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public abstract class StoredElement implements Element, CacheEntry {

    long cache = Instant.now().plus(30, ChronoUnit.SECONDS).toEpochMilli();
    boolean preserve = false;

    public StoredElement(@NotNull JsonObject data) {

    }

    public abstract void save();

    public void scheduleSave() {
        Rainbow.DAO.async(this::save);
    }

    public abstract Class<? extends StoredElement> getCreationClass();

    public abstract @NotNull JsonObject getAsJson();

    @Override
    public String toString() {
        return getAsJson().toString();
    }

    protected List<MetaComponent> deserialiseMeta(JsonArray array) {
        List<MetaComponent> list = new ArrayList<>();
        for (JsonElement element : array) {
            MetaComponent component = MetaComponent.fromJson(element);
            list.add(component);
        }
        return list;
    }

    protected JsonArray serialiseMeta(List<MetaComponent> list) {
        JsonArray array = new JsonArray();
        for (MetaComponent value : list) {
            array.add(value.getAsJson());
        }
        return array;
    }

    @Override
    public long killTime() {
        return cache;
    }

    @Override
    public boolean preserve() {
        return preserve;
    }

    @Override
    public void setPreserved(boolean boo) {
        preserve = boo;
    }

    @Override
    public void preserve(long epochMilli) {
        cache = epochMilli;
    }

}
