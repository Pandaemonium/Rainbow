package dev.moderocky.rainbow.api.element;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface Group extends Element, Inheritor<Group>, VisualElement, Weighted, Permissible {

    @NotNull String getID();

    String getDisplayName();

    void setDisplayName(@Nullable String string);

    default @NotNull String getEffectiveName() {
        String name = getDisplayName();
        return name != null ? name : getID();
    }

    boolean hasMembers();

    boolean isDefault();

    void recalculateInheritance();

    void recalculatePermissions();

}
