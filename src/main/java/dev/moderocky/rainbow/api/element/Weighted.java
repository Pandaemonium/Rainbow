package dev.moderocky.rainbow.api.element;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface Weighted {

    static <T extends Weighted> @Unmodifiable Collection<T> sort(final Collection<T> entries) {
        final List<T> list = new ArrayList<>(entries);
        list.sort(Comparator.comparing(Weighted::getWeight));
        return list;
    }

    int getWeight();

    void setWeight(@Nullable Integer weight);

    default boolean overrides(@Nullable Weighted weighted) {
        if (weighted == null) return true;
        return getWeight() > weighted.getWeight();
    }

}
