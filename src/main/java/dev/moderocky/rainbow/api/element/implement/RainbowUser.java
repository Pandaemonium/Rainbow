package dev.moderocky.rainbow.api.element.implement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.*;
import dev.moderocky.rainbow.api.internal.JSerialiser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.*;

public class RainbowUser extends StoredElement implements User, JSerialiser {

    final UUID uuid;
    final List<MetaComponent> meta;
    final List<Group> groups;
    final Map<String, Boolean> permissions;
    final Map<String, Boolean> effectivePermissions;
    @Nullable String name;
    Group primaryGroup;
    boolean joined;

    public RainbowUser(@NotNull JsonObject data) {
        super(data);
        try {
            uuid = UUID.fromString(data.get("uuid").getAsString());
            name = data.get("name").getAsString();
            meta = deserialiseMeta(data.get("meta").getAsJsonArray());
            groups = toGroups(getAsStrings(data.getAsJsonArray("groups")));
            permissions = getAsBooleanMap(data.getAsJsonArray("permissions"), new HashMap<>());
            effectivePermissions = new HashMap<>();
        } catch (Throwable throwable) {
            throw new IllegalArgumentException(throwable);
        }
        recalculatePermissions();
        recalculatePrimaryGroup();
    }

    public RainbowUser(UUID uuid) {
        super(new JsonObject());
        this.uuid = uuid;
        this.name = null;
        meta = new ArrayList<>();
        groups = new ArrayList<>();
        permissions = new HashMap<>();
        effectivePermissions = new HashMap<>();
        recalculatePermissions();
        recalculatePrimaryGroup();
    }

    @Override
    public void save() {
        Rainbow.DAO.save(this);
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public Group getPrimaryGroup() {
        return primaryGroup;
    }

    @Override
    public void setPrimaryGroup(Group group) {
        this.primaryGroup = group;
        addGroup(group);
    }

    @Override
    public @Unmodifiable List<Group> getGroups() {
        if (!groups.contains(Rainbow.getDefaultGroup())) groups.add(Rainbow.getDefaultGroup());
        return new ArrayList<>(groups);
    }

    @Override
    public boolean hasJoined() {
        return joined;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Override
    public void addGroup(Group group) {
        if (!groups.contains(group)) groups.add(group);
    }

    @Override
    public void removeGroup(Group group) {
        groups.remove(group);
    }

    @Override
    public void recalculatePrimaryGroup() {
        final List<Group> groups = getGroups();
        Group group = Rainbow.getDefaultGroup();
        for (Group sample : groups) {
            if (sample.overrides(group)) group = sample;
        }
        primaryGroup = group;
    }

    @Override
    public void recalculatePermissions() {
        effectivePermissions.clear();
        for (Group group : getWeightedGroupOrder()) {
            effectivePermissions.putAll(group.getEffectiveFinalPermissions());
        }
        effectivePermissions.putAll(permissions);
    }

    @Override
    public @Unmodifiable JsonArray getMetadata() {
        return serialiseMeta(meta);
    }

    @Override
    public @Unmodifiable List<MetaComponent> getMetaEntries() {
        return new ArrayList<>(meta);
    }

    @Override
    public void addMetaEntry(MetaComponent component) {
        meta.add(component);
    }

    @Override
    public void removeMetaEntries(String key) {
        meta.removeIf(filter -> filter.getKey().equalsIgnoreCase(key));
    }

    @Override
    public void removeMetaEntry(MetaComponent component) {
        meta.remove(component);
    }

    @Override
    public Class<? extends StoredElement> getCreationClass() {
        return this.getClass();
    }

    @Override
    public @NotNull JsonObject getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("uuid", uuid.toString());
        object.addProperty("name", name);
        object.add("meta", serialiseMeta(meta));
        object.add("groups", toArray(groups));
        object.add("permissions", toArray(permissions));
        return object;
    }

    private List<Group> getWeightedGroupOrder() {
        List<Group> groups = new ArrayList<>(getGroups());
        groups.sort(Comparator.comparingInt(Weighted::getWeight));
        return groups;
    }

    @Override
    public void setPermission(String permission, boolean value) {
        permissions.put(permission, value);
    }

    @Override
    public void removePermission(String permission) {
        permissions.remove(permission);
    }

    @Override
    public boolean hasPermission(String permission) {
        return permissions.getOrDefault(permission, false);
    }

    @Override
    public boolean isPermissionSet(String permission) {
        return permissions.containsKey(permission);
    }

    @Override
    public Collection<String> getPermissions() {
        return new ArrayList<>(permissions.keySet());
    }

    @Override
    public boolean hasFinalPermission(String permission) {
        return effectivePermissions.getOrDefault(permission, false);
    }

    @Override
    public boolean isFinalPermissionSet(String permission) {
        return effectivePermissions.containsKey(permission);
    }

    @Override
    public @Unmodifiable Collection<String> getFinalPermissions() {
        return effectivePermissions.keySet();
    }

    @Override
    public @Unmodifiable Map<String, Boolean> getEffectiveFinalPermissions() {
        return effectivePermissions;
    }
}
