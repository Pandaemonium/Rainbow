package dev.moderocky.rainbow.api.element;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;

public class MetaComponent implements Weighted {

    private final String key;
    private @Nullable String value = null;
    private int weight = 0;

    public MetaComponent(String key) {
        this.key = key;
    }

    public MetaComponent(String key, @Nullable String value) {
        this.key = key;
        this.value = value;
    }

    public MetaComponent(String key, @Nullable String value, @Nullable Integer weight) {
        this.key = key;
        this.value = value;
        this.weight = (weight == null || weight < 0) ? 0 : weight;
    }

    public static MetaComponent fromString(String string) {
        return fromJson(Rainbow.JSON_PARSER.parse(string));
    }

    public static MetaComponent fromJson(JsonElement element) {
        JsonObject object = element.getAsJsonObject();
        String key = null;
        String value = null;
        int weight = 0;
        for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("key")) key = entry.getValue().toString();
            else if (entry.getKey().equalsIgnoreCase("value")) value = entry.getValue().toString();
            else if (entry.getKey().equalsIgnoreCase("weight")) weight = entry.getValue().getAsInt();
        }
        if (key == null) throw new IllegalArgumentException();
        return new MetaComponent(key, value, weight);
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(@Nullable Integer weight) {
        this.weight = (weight == null || weight < 0) ? 0 : weight;
    }

    public String getKey() {
        return key;
    }

    public @Nullable String getValue() {
        return value;
    }

    public void setValue(@Nullable String value) {
        this.value = value;
    }

    public boolean isNull() {
        return value == null;
    }

    public boolean overrides(@Nullable MetaComponent component) {
        if (component == null) return true;
        return component.getKey().equalsIgnoreCase(key) && weight > component.getWeight();
    }

    public JsonElement getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("key", key);
        object.addProperty("value", value);
        object.addProperty("weight", weight);
        return object;
    }

    @Override
    public String toString() {
        return getAsJson().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetaComponent)) return false;
        MetaComponent component = (MetaComponent) o;
        return Objects.equals(key, component.key) &&
                Objects.equals(value, component.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value, weight);
    }

}
