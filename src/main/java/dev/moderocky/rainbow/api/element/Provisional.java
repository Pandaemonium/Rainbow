package dev.moderocky.rainbow.api.element;

import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.implement.ProvisionalGroup;
import dev.moderocky.rainbow.api.element.implement.ProvisionalUser;
import dev.moderocky.rainbow.api.storage.CacheDAO;

import java.util.function.Consumer;

public interface Provisional<T extends Element> {

    default CacheDAO dao() {
        return Rainbow.DAO.getCache();
    }

    default T getSuper() {
        if (this instanceof Group) {
            return (T) dao().getSource().getGroup(((Group) this).getID());
        } else if (this instanceof User) {
            return (T) dao().getSource().getUser(((User) this).getUUID());
        }
        return null;
    }

    default void upstream(Consumer<Group> consumer, ProvisionalGroup group) {
        dao().getGroupAsync(group.getID(), consumer);
    }

    default void upstream(Consumer<User> consumer, ProvisionalUser user) {
        dao().getUserAsync(user.getUUID(), consumer);
    }

}
