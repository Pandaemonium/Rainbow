package dev.moderocky.rainbow.api.element;

import dev.moderocky.rainbow.api.Rainbow;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

/**
 * An element that can have visual data aspects, such as a user or a group.
 */
public interface VisualElement extends Element {

    static boolean isComponent(String string) {
        try {
            Rainbow.JSON_PARSER.parse(string);
            return true;
        } catch (Throwable throwable) {
            return false;
        }
    }

    static BaseComponent[] getComponent(String string) {
        if (isComponent(string)) return ComponentSerializer.parse(string);
        return TextComponent.fromLegacyText(string);
    }

    static String getLegacy(String string) {
        if (isComponent(string)) return TextComponent.toLegacyText(ComponentSerializer.parse(string));
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    default boolean hasPrefix() {
        return hasMetaEntry("prefix");
    }

    default String getLegacyPrefix() {
        return getLegacy(getTopMetaValue("prefix"));
    }

    default BaseComponent[] getPrefix() {
        return getComponent(getTopMetaValue("prefix"));
    }

    default boolean hasSuffix() {
        return hasMetaEntry("suffix");
    }

    default String getLegacySuffix() {
        return getLegacy(getTopMetaValue("suffix"));
    }

    default BaseComponent[] getSuffix() {
        return getComponent(getTopMetaValue("suffix"));
    }

}
