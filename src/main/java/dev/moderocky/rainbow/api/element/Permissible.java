package dev.moderocky.rainbow.api.element;

import org.jetbrains.annotations.Unmodifiable;

import java.util.Collection;
import java.util.Map;

public interface Permissible {

    default void setPermission(String permission) {
        if (permission.startsWith("-")) setPermission(permission.substring(1), false);
        else setPermission(permission, true);
    }

    void setPermission(String permission, boolean value);

    void removePermission(String permission);

    boolean hasPermission(String permission);

    boolean isPermissionSet(String permission);

    @Unmodifiable Collection<String> getPermissions();

    boolean hasFinalPermission(String permission);

    boolean isFinalPermissionSet(String permission);

    @Unmodifiable Collection<String> getFinalPermissions();

    @Unmodifiable Map<String, Boolean> getEffectiveFinalPermissions();

}
