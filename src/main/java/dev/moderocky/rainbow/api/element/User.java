package dev.moderocky.rainbow.api.element;

import dev.moderocky.rainbow.api.Rainbow;

import java.util.List;
import java.util.UUID;

public interface User extends Element, VisualElement, Permissible {

    UUID getUUID();

    Group getPrimaryGroup();

    void setPrimaryGroup(Group group);

    List<Group> getGroups();

    boolean hasJoined();

    default boolean hasGroup(Group group) {
        return getGroups().contains(group);
    }

    default boolean hasGroup(String id) {
        Group group = Rainbow.DAO.getCache().getProvisionalGroup(id);
        if (group == null) return false;
        return getGroups().contains(group);
    }

    String getName();

    void setName(String name);

    void addGroup(Group group);

    void removeGroup(Group group);

    void recalculatePrimaryGroup();

    void recalculatePermissions();

}
