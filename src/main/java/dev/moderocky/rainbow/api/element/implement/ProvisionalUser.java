package dev.moderocky.rainbow.api.element.implement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.annotation.Async;
import dev.moderocky.rainbow.api.annotation.BlockingAction;
import dev.moderocky.rainbow.api.annotation.ClassMirror;
import dev.moderocky.rainbow.api.annotation.Sync;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.MetaComponent;
import dev.moderocky.rainbow.api.element.Provisional;
import dev.moderocky.rainbow.api.element.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@ClassMirror(mirror = RainbowUser.class)
public class ProvisionalUser implements User, Provisional<User> {

    public UUID uuid;
    public String name;
    public Group primaryGroup;
    public boolean joined;

    @Override
    @Sync
    public UUID getUUID() {
        return uuid;
    }

    @Override
    @Sync
    public Group getPrimaryGroup() {
        return primaryGroup;
    }

    @Override
    @Async
    public void setPrimaryGroup(Group group) {
        upstream(user -> user.setPrimaryGroup(group), this);
    }

    @Override
    @BlockingAction
    public List<Group> getGroups() {
        return getSuper().getGroups();
    }

    @Override
    @Sync
    public boolean hasJoined() {
        return joined;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @Async
    public void setName(String name) {
        this.name = name;
        upstream(user -> user.setName(name), this);
    }

    @Override
    @Async
    public void addGroup(Group group) {
        upstream(user -> user.addGroup(group), this);
    }

    @Override
    @Async
    public void removeGroup(Group group) {
        upstream(user -> user.removeGroup(group), this);
    }

    @Override
    @Async
    public void recalculatePrimaryGroup() {
        upstream(User::recalculatePrimaryGroup, this);
    }

    @Override
    @Async
    public void recalculatePermissions() {
        upstream(User::recalculatePermissions, this);
    }

    @Override
    @Async
    public void setPermission(String permission, boolean value) {
        upstream(user -> user.setPermission(permission, value), this);
    }

    @Override
    @Async
    public void removePermission(String permission) {
        upstream(user -> user.removePermission(permission), this);
    }

    @Override
    @BlockingAction
    public boolean hasPermission(String permission) {
        return getSuper().hasPermission(permission);
    }

    @Override
    @BlockingAction
    public boolean isPermissionSet(String permission) {
        return getSuper().isPermissionSet(permission);
    }

    @Override
    @BlockingAction
    public @Unmodifiable Collection<String> getPermissions() {
        return getSuper().getPermissions();
    }

    @Override
    @BlockingAction
    public boolean hasFinalPermission(String permission) {
        return getSuper().hasFinalPermission(permission);
    }

    @Override
    @BlockingAction
    public boolean isFinalPermissionSet(String permission) {
        return getSuper().isFinalPermissionSet(permission);
    }

    @Override
    @BlockingAction
    public @Unmodifiable Collection<String> getFinalPermissions() {
        return getSuper().getFinalPermissions();
    }

    @Override
    @BlockingAction
    public @Unmodifiable Map<String, Boolean> getEffectiveFinalPermissions() {
        return getSuper().getEffectiveFinalPermissions();
    }

    @Override
    @BlockingAction
    public JsonArray getMetadata() {
        return getSuper().getMetadata();
    }

    @Override
    @BlockingAction
    public List<MetaComponent> getMetaEntries() {
        return getSuper().getMetaEntries();
    }

    @Override
    @Async
    public void addMetaEntry(MetaComponent component) {
        upstream(user -> user.addMetaEntry(component), this);
    }

    @Override
    @Async
    public void removeMetaEntries(String key) {
        upstream(user -> user.removeMetaEntries(key), this);
    }

    @Override
    @Async
    public void removeMetaEntry(MetaComponent component) {
        upstream(user -> user.removeMetaEntry(component), this);
    }

    @Override
    @BlockingAction
    public @NotNull JsonObject getAsJson() {
        return getSuper().getAsJson();
    }
}
