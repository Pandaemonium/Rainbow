package dev.moderocky.rainbow.api.element.implement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.MetaComponent;
import dev.moderocky.rainbow.api.element.StoredElement;
import dev.moderocky.rainbow.api.element.Weighted;
import dev.moderocky.rainbow.api.internal.JSerialiser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.*;

public class RainbowGroup extends StoredElement implements Group, JSerialiser {

    final String id;
    final List<MetaComponent> meta;
    final Map<String, Boolean> permissions;
    final Map<String, Boolean> effectivePermissions;
    final List<Group> ancestors;
    final List<Group> descendants;
    @Nullable String name;
    int weight;
    boolean empty;
    boolean def;

    public RainbowGroup(@NotNull JsonObject data) {
        super(data);
        try {
            id = data.get("id").getAsString();
            name = data.get("name").getAsString();
            meta = deserialiseMeta(data.get("meta").getAsJsonArray());
            weight = data.get("weight").getAsInt();
            def = data.get("default").getAsBoolean();
            ancestors = toGroups(getAsStrings(data.getAsJsonArray("ancestors")));
            descendants = toGroups(getAsStrings(data.getAsJsonArray("descendants")));
            permissions = getAsBooleanMap(data.getAsJsonArray("permissions"), new HashMap<>());
            effectivePermissions = new HashMap<>();
        } catch (Throwable throwable) {
            throw new IllegalArgumentException(throwable);
        }
        recalculatePermissions();
        recalculateInheritance();
    }

    public RainbowGroup(@NotNull String id) {
        super(new JsonObject());
        this.id = id;
        this.name = null;
        weight = 0;
        def = false;
        meta = new ArrayList<>();
        permissions = new HashMap<>();
        effectivePermissions = new HashMap<>();
        ancestors = new ArrayList<>();
        descendants = new ArrayList<>();
        recalculatePermissions();
        recalculateInheritance();
    }

    @Override
    public void save() {
        Rainbow.DAO.save(this);
    }

    @Override
    public @NotNull String getID() {
        return id;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public void setDisplayName(@Nullable String string) {
        this.name = string;
    }

    @Override
    public boolean hasMembers() {
        return !empty;
    }

    @Override
    public boolean isDefault() {
        return def;
    }

    public void setDefault(boolean boo) {
        def = boo;
    }

    @Override
    public void recalculateInheritance() {
        List<Group> groups = new ArrayList<>(ancestors);
        groups.removeIf(group -> !Rainbow.DAO.groupExists(group.getID()));
        ancestors.clear();
        ancestors.addAll(groups);
    }

    @Override
    public void recalculatePermissions() {
        effectivePermissions.clear();
        for (Group group : getWeightedGroupOrder()) {
            effectivePermissions.putAll(group.getEffectiveFinalPermissions());
        }
        effectivePermissions.putAll(permissions);
    }

    @Override
    public Class<? extends StoredElement> getCreationClass() {
        return this.getClass();
    }

    @Override
    public boolean hasAncestors() {
        return !ancestors.isEmpty();
    }

    @Override
    public @Unmodifiable List<Group> getAncestors() {
        return ancestors;
    }

    @Override
    public boolean hasDescendants() {
        return !descendants.isEmpty();
    }

    @Override
    public @Unmodifiable List<Group> getDescendants() {
        return descendants;
    }

    @Override
    public @Unmodifiable JsonArray getMetadata() {
        return serialiseMeta(meta);
    }

    @Override
    public @Unmodifiable List<MetaComponent> getMetaEntries() {
        return new ArrayList<>(meta);
    }

    @Override
    public void addMetaEntry(MetaComponent component) {
        meta.add(component);
    }

    @Override
    public void removeMetaEntries(String key) {
        meta.removeIf(filter -> filter.getKey().equalsIgnoreCase(key));
    }

    @Override
    public void removeMetaEntry(MetaComponent component) {
        meta.remove(component);
    }

    @Override
    public @NotNull JsonObject getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("id", id);
        object.addProperty("name", name);
        object.addProperty("weight", weight);
        object.addProperty("default", def);
        object.add("meta", serialiseMeta(meta));
        object.add("ancestors", toArray(ancestors));
        object.add("descendants", toArray(descendants));
        object.add("permissions", toArray(permissions));
        return object;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof String) {
            return Objects.equals(id, o);
        } else {
            if (!(o instanceof Group)) return false;
            Group that = (Group) o;
            return Objects.equals(id, that.getID());
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public void setPermission(String permission, boolean value) {
        permissions.put(permission, value);
    }

    @Override
    public void removePermission(String permission) {
        permissions.remove(permission);
    }

    @Override
    public boolean hasPermission(String permission) {
        return permissions.getOrDefault(permission, false);
    }

    @Override
    public boolean isPermissionSet(String permission) {
        return permissions.containsKey(permission);
    }

    @Override
    public @Unmodifiable Collection<String> getPermissions() {
        return permissions.keySet();
    }

    @Override
    public boolean hasFinalPermission(String permission) {
        return effectivePermissions.getOrDefault(permission, false);
    }

    @Override
    public boolean isFinalPermissionSet(String permission) {
        return effectivePermissions.containsKey(permission);
    }

    @Override
    public @Unmodifiable Collection<String> getFinalPermissions() {
        return effectivePermissions.keySet();
    }

    @Override
    public @Unmodifiable Map<String, Boolean> getEffectiveFinalPermissions() {
        return effectivePermissions;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(@Nullable Integer weight) {
        if (weight == null) weight = 0;
        this.weight = Math.max(0, weight);
    }


    private List<Group> getWeightedGroupOrder() {
        List<Group> groups = new ArrayList<>(getAncestors());
        groups.sort(Comparator.comparingInt(Weighted::getWeight));
        return groups;
    }
}
