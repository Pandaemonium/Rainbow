package dev.moderocky.rainbow.api.element;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.Rainbow;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * An object that can have permission data.
 * This includes users, groups, etc.
 */
public interface Element {

    JsonArray getMetadata();

    List<MetaComponent> getMetaEntries();

    void addMetaEntry(MetaComponent component);

    void removeMetaEntries(String key);

    void removeMetaEntry(MetaComponent component);

    default boolean hasMetaEntry(String key) {
        final List<MetaComponent> meta = getMetaEntries();
        for (MetaComponent component : meta) {
            if (component.getKey().equalsIgnoreCase(key)) return true;
        }
        return false;
    }

    default @NotNull Collection<MetaComponent> getMetaEntries(String key) {
        final List<MetaComponent> meta = getMetaEntries();
        final Set<MetaComponent> entries = new HashSet<>();
        for (MetaComponent component : meta) {
            if (component.getKey().equalsIgnoreCase(key)) entries.add(component);
        }
        return entries;
    }

    default @NotNull Collection<String> getMetaValues(String key) {
        final List<String> values = new ArrayList<>();
        for (MetaComponent component : getMetaEntries(key)) {
            values.add(component.getValue());
        }
        return values;
    }

    default MetaComponent getTopMetaEntry(String key) {
        MetaComponent query = null;
        for (MetaComponent component : getMetaEntries(key)) {
            if (component.overrides(query)) query = component;
        }
        return query;
    }

    default String getTopMetaValue(String key) {
        MetaComponent query = getTopMetaEntry(key);
        return query != null ? query.getValue() : null;
    }

    @NotNull JsonObject getAsJson();

    String toString();

    default byte[] compress() {
        return Rainbow.COMPACTOR.zip(getAsJson().toString());
    }

}
