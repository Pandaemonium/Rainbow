package dev.moderocky.rainbow.api.element.implement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dev.moderocky.rainbow.api.annotation.Async;
import dev.moderocky.rainbow.api.annotation.BlockingAction;
import dev.moderocky.rainbow.api.annotation.ClassMirror;
import dev.moderocky.rainbow.api.annotation.Sync;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.MetaComponent;
import dev.moderocky.rainbow.api.element.Provisional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@ClassMirror(mirror = RainbowGroup.class)
public class ProvisionalGroup implements Group, Provisional<Group> {

    public String id;
    public String name;
    public boolean members;
    public boolean def;

    @Override
    @Sync
    public @NotNull String getID() {
        return id;
    }

    @Override
    @Sync
    public String getDisplayName() {
        return name;
    }

    @Override
    @Async
    public void setDisplayName(@Nullable String string) {
        upstream(group -> group.setDisplayName(string), this);
    }

    @Override
    @Sync
    public boolean hasMembers() {
        return members;
    }

    @Override
    @Sync
    public boolean isDefault() {
        return def;
    }

    @Override
    @Async
    public void recalculateInheritance() {
        upstream(Group::recalculateInheritance, this);
    }

    @Override
    @Async
    public void recalculatePermissions() {
        upstream(Group::recalculatePermissions, this);
    }

    @Override
    @BlockingAction
    public JsonArray getMetadata() {
        return getSuper().getMetadata();
    }

    @Override
    @BlockingAction
    public List<MetaComponent> getMetaEntries() {
        return getSuper().getMetaEntries();
    }

    @Override
    @Async
    public void addMetaEntry(MetaComponent component) {
        upstream(group -> group.addMetaEntry(component), this);
    }

    @Override
    @Async
    public void removeMetaEntries(String key) {
        upstream(group -> group.removeMetaEntries(key), this);
    }

    @Override
    @Async
    public void removeMetaEntry(MetaComponent component) {
        upstream(group -> group.removeMetaEntry(component), this);
    }

    @Override
    @BlockingAction
    public @NotNull JsonObject getAsJson() {
        return getSuper().getAsJson();
    }

    @Override
    @BlockingAction
    public boolean hasAncestors() {
        return getSuper().hasAncestors();
    }

    @Override
    @BlockingAction
    public List<Group> getAncestors() {
        return getSuper().getAncestors();
    }

    @Override
    @BlockingAction
    public boolean hasDescendants() {
        return getSuper().hasDescendants();
    }

    @Override
    @BlockingAction
    public List<Group> getDescendants() {
        return getSuper().getDescendants();
    }

    @Override
    @Async
    public void setPermission(String permission, boolean value) {
        upstream(group -> group.setPermission(permission, value), this);
    }

    @Override
    @Async
    public void removePermission(String permission) {
        upstream(group -> group.removePermission(permission), this);
    }

    @Override
    @BlockingAction
    public boolean hasPermission(String permission) {
        return getSuper().hasPermission(permission);
    }

    @Override
    @BlockingAction
    public boolean isPermissionSet(String permission) {
        return getSuper().isPermissionSet(permission);
    }

    @Override
    @BlockingAction
    public @Unmodifiable Collection<String> getPermissions() {
        return getSuper().getPermissions();
    }

    @Override
    @BlockingAction
    public boolean hasFinalPermission(String permission) {
        return getSuper().hasFinalPermission(permission);
    }

    @Override
    @BlockingAction
    public boolean isFinalPermissionSet(String permission) {
        return getSuper().isFinalPermissionSet(permission);
    }

    @Override
    @BlockingAction
    public @Unmodifiable Collection<String> getFinalPermissions() {
        return getSuper().getFinalPermissions();
    }

    @Override
    @BlockingAction
    public @Unmodifiable Map<String, Boolean> getEffectiveFinalPermissions() {
        return getSuper().getEffectiveFinalPermissions();
    }

    @Override
    @BlockingAction
    public int getWeight() {
        return getSuper().getWeight();
    }

    @Override
    @Async
    public void setWeight(@Nullable Integer weight) {
        upstream(group -> group.setWeight(weight), this);
    }
}
