package dev.moderocky.rainbow.api;

import com.google.gson.JsonParser;
import dev.moderocky.rainbow.api.annotation.*;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.StoredElement;
import dev.moderocky.rainbow.api.element.User;
import dev.moderocky.rainbow.api.element.implement.RainbowGroup;
import dev.moderocky.rainbow.api.internal.AttachmentMirror;
import dev.moderocky.rainbow.api.internal.CoreConstant;
import dev.moderocky.rainbow.api.storage.Compactor;
import dev.moderocky.rainbow.api.storage.DAO;
import dev.moderocky.rainbow.api.storage.file.JsonFileTreeDAO;
import dev.moderocky.rainbow.api.storage.mongo.MongoDatabaseDAO;
import dev.moderocky.rainbow.common.RainbowConfig;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Rainbow {

    public static final JsonParser JSON_PARSER = new JsonParser();
    public static final Compactor COMPACTOR = new Compactor();
    public static final IPermissionParser CHECKER = new RegexPermissionParser();
    public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(6);
    public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(1);
    public static final BaseComponent[] PREFIX = new ComponentBuilder("")
            .color(ChatColor.WHITE)
            .append("‹").color(ChatColor.DARK_GRAY)
            .append("R").color(ChatColor.RED)
            .append("a").color(ChatColor.GOLD)
            .append("i").color(ChatColor.YELLOW)
            .append("n").color(ChatColor.GREEN)
            .append("b").color(ChatColor.DARK_AQUA)
            .append("o").color(ChatColor.DARK_PURPLE)
            .append("w").color(ChatColor.LIGHT_PURPLE)
            .append("›").color(ChatColor.DARK_GRAY)
            .append(" ").color(ChatColor.WHITE)
            .create();
    public static @EffectivelyFinal
    DAO DAO;
    private static Rainbow rainbow = null;

    private final RainbowAPI api = new RainbowAPI();
    private final RainbowConfig config = new RainbowConfig();
    private final CoreConstant plugin;
    private final HashMap<String, AttachmentMirror> mirrorMap = new HashMap<>();

    public Rainbow(CoreConstant plugin) {
        if (rainbow != null) throw new IllegalArgumentException();
        rainbow = this;
        this.plugin = plugin;
    }

    public static Group getDefaultGroup() {
        RainbowGroup group = (RainbowGroup) DAO.getOrCreateGroup("default");
        if (!group.isDefault()) group.setDefault(true);
        return DAO.getOrCreateGroup("default");
    }

    public static Rainbow getInstance() {
        return rainbow;
    }

    public static Rainbow.RainbowAPI getAPI() {
        return rainbow.api;
    }

    public void startup() {
        reload();
    }

    public void shutdown() {
        DAO.shutdown();
        mirrorMap.clear();
        rainbow = null;
        DAO = null;
    }

    public void reload() {
        mirrorMap.clear();
        if (config.storageType.equalsIgnoreCase("file")) {
            DAO = new JsonFileTreeDAO();
        } else if (config.storageType.contains("mongo")) {
            DAO = new MongoDatabaseDAO();
        }
    }

    public CoreConstant getPlugin() {
        return plugin;
    }

    public interface IPermissionParser {

        boolean matches(String permission, String target);

    }

    static class RegexPermissionParser implements IPermissionParser {

        protected RegexPermissionParser() {

        }

        @Override
        public boolean matches(String permission, String target) {
            String test = permission;
            boolean invert = test.startsWith("-");
            if (invert) test = test.substring(1);
            if ((test.startsWith("r:") && test.length() > 2) || (test.endsWith(".*") || test.equalsIgnoreCase("*"))) {
                if ((test.startsWith("r:") && test.length() > 2)) test = test.substring(2);
                if (test.endsWith(".*") || test.equalsIgnoreCase("*"))
                    test = test.substring(0, test.length() - 1) + ".+";
                if (!test.startsWith("^")) test = "^" + test;
                if (!test.endsWith("$")) test = test + "$";
                Pattern pattern = Pattern.compile(test);
                Matcher matcher = pattern.matcher(target);
                return matcher.matches() ^ invert;
            } else {
                return test.equalsIgnoreCase(target) ^ invert;
            }
        }

    }

    protected class RainbowAPI {

        public void reload() {
            Rainbow.this.reload();
        }

        @Sync
        public boolean userExists(UUID uuid) {
            return DAO.userExists(uuid);
        }

        @Sync
        public boolean userExists(OfflinePlayer player) {
            return DAO.userExists(player.getUniqueId());
        }

        @Sync
        public User getUser(org.bukkit.entity.Player player) {
            return DAO.getUser(player.getUniqueId());
        }

        @Sync
        public User getUser(org.bukkit.OfflinePlayer player) {
            return DAO.getUser(player.getUniqueId());
        }

        @Sync
        public User getFunctionalUser(org.bukkit.OfflinePlayer player) {
            return DAO.getCache().getFunctionalUser(player.getUniqueId());
        }

        @Async
        @FutureAction
        public void getUserAsync(org.bukkit.OfflinePlayer player, Consumer<User> consumer) {
            DAO.getUserAsync(player.getUniqueId(), consumer);
        }

        @Sync
        public User getUser(net.md_5.bungee.api.connection.ProxiedPlayer player) {
            return DAO.getUser(player.getUniqueId());
        }

        @Sync
        @BlockingAction
        public User getUser(UUID uuid) {
            return DAO.getUser(uuid);
        }

        @Sync
        public User getFunctionalUser(UUID uuid) {
            return DAO.getCache().getFunctionalUser(uuid);
        }

        @Async
        @FutureAction
        public void getUserAsync(UUID uuid, Consumer<User> consumer) {
            DAO.getUserAsync(uuid, consumer);
        }

        @Sync
        @BlockingAction
        public User getOrCreateUser(UUID uuid) {
            return DAO.getOrCreateUser(uuid);
        }

        @Async
        @FutureAction
        @SuppressWarnings("unchecked")
        public void createUser(UUID uuid, Consumer<User>... consumers) {
            DAO.async(() -> {
                User user = getOrCreateUser(uuid);
                for (Consumer<User> consumer : consumers) {
                    consumer.accept(user);
                }
                ((StoredElement) user).save();
            });
        }

        @Sync
        @BlockingAction
        public Group getGroup(String id) {
            return DAO.getCache().getFunctionalGroup(id);
        }

        @Sync
        public RainbowGroup getFunctionalGroup(String id) {
            return (RainbowGroup) DAO.getGroup(id);
        }

        @Async
        @FutureAction
        public void getGroupAsync(String id, Consumer<Group> consumer) {
            DAO.getGroupAsync(id, consumer);
        }

        @Sync
        @BlockingAction
        public Group getOrCreateGroup(String id) {
            return DAO.getOrCreateGroup(id);
        }

        @Async
        @FutureAction
        @SuppressWarnings("unchecked")
        public void createGroup(String id, Consumer<Group>... consumers) {
            Group group = getOrCreateGroup(id);
            for (Consumer<Group> consumer : consumers) {
                consumer.accept(group);
            }
            ((StoredElement) group).save();
        }

    }

}
