package dev.moderocky.rainbow.api.storage;

import dev.moderocky.rainbow.api.annotation.Async;
import dev.moderocky.rainbow.api.annotation.Sync;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.StoredElement;
import dev.moderocky.rainbow.api.element.User;
import dev.moderocky.rainbow.api.element.implement.ProvisionalGroup;
import dev.moderocky.rainbow.api.element.implement.ProvisionalUser;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public interface CacheDAO extends DAO {

    @Sync
    default User getFunctionalUser(UUID uuid) {
        if (hasUser(uuid)) return getUser(uuid);
        return getProvisionalUser(uuid);
    }

    @Sync
    default Group getFunctionalGroup(String id) {
        if (hasGroup(id)) return getGroup(id);
        return getProvisionalGroup(id);
    }

    @Sync
    ProvisionalUser getProvisionalUser(UUID uuid);

    @Sync
    ProvisionalGroup getProvisionalGroup(String id);

    @Sync
    boolean hasUser(@NotNull UUID uuid);

    @Sync
    boolean hasGroup(@NotNull String id);

    @Sync
    default void cacheUser(@NotNull UUID uuid) {
        cacheUser(getSource().getOrCreateUser(uuid));
    }

    @Sync
    default void cacheGroup(@NotNull String id) {
        cacheGroup(getSource().getOrCreateGroup(id));
    }

    @Sync
    void cacheUser(@NotNull User user);

    @Sync
    void removeCache(@NotNull User user);

    @Sync
    void clearUserCache();

    @Sync
    void cacheGroup(@NotNull Group group);

    @Sync
    void removeCache(@NotNull Group group);

    @Sync
    void clearGroupCache();

    @Sync
    default void clearCaches() {
        clearUserCache();
        clearGroupCache();
    }

    @Sync
    User getUser(@NotNull UUID uuid);

    @Sync
    Group getGroup(@NotNull String id);

    @Sync
    @NotNull
    List<Group> getGroups();

    @Sync
    @NotNull
    List<User> getUsers();

    @Sync
    @NotNull
    default List<Group> getCachedGroups() {
        return getGroups();
    }

    @Sync
    @NotNull
    default List<User> getCachedUsers() {
        return getUsers();
    }

    @Override
    default void save(User user) {
        getSource().save(user);
    }

    @Override
    default void save(Group group) {
        getSource().save(group);
    }

    default void async(Runnable runnable) {
        getSource().async(runnable);
    }

    default void scheduleSave(StoredElement element) {
        getSource().scheduleSave(element);
    }

    @Async
    default void purge() {
        async(this::purgeUserCache);
        async(this::purgeGroupCache);
    }

    @Sync
    default void purgeUserCache() {
        for (User user : getCachedUsers()) {
            if (!(user instanceof StoredElement)) continue;
            StoredElement element = (StoredElement) user;
            if (element.isExpired()) removeCache(user);
        }
    }

    @Sync
    default void purgeGroupCache() {
        for (Group group : getCachedGroups()) {
            if (!(group instanceof StoredElement)) continue;
            StoredElement element = (StoredElement) group;
            if (element.isExpired()) removeCache(group);
        }
    }

    default CacheDAO getCache() {
        return this;
    }

    @Override
    @NotNull
    DAO getSource();

    @Override
    default @NotNull User getOrCreateUser(@NotNull UUID uuid) {
        return getSource().getOrCreateUser(uuid);
    }

    @Override
    default @NotNull Group getOrCreateGroup(@NotNull String id) {
        return getSource().getOrCreateGroup(id);
    }

    @Override
    default boolean userExists(@NotNull UUID uuid) {
        return getSource().userExists(uuid);
    }

    @Override
    default boolean groupExists(@NotNull String id) {
        return getSource().groupExists(id);
    }

    @Override
    default void getUserAsync(@NotNull UUID uuid, @NotNull Consumer<User> completable) {
        getSource().getUserAsync(uuid, completable);
    }

    @Override
    default void getGroupAsync(@NotNull String id, @NotNull Consumer<Group> completable) {
        getSource().getGroupAsync(id, completable);
    }

    @Override
    default void getUsersAsync(@NotNull Consumer<List<User>> completable) {
        getSource().getUsersAsync(completable);
    }

    @Override
    default void getGroupsAsync(@NotNull Consumer<List<Group>> completable) {
        getSource().getGroupsAsync(completable);
    }

}
