package dev.moderocky.rainbow.api.storage;

import java.time.Instant;

public interface CacheEntry {

    /**
     * @return The epoch milli of when this entry should expire
     */
    long killTime();

    /**
     * @return Whether this should be preserved despite expiry
     */
    boolean preserve();

    /**
     * @param boo The protected status of this entry
     */
    void setPreserved(boolean boo);

    /**
     * @param epochMilli The new expiry epoch
     */
    void preserve(long epochMilli);

    /**
     * @return Whether this is unprotected and expired
     */
    default boolean isExpired() {
        return !preserve() || Instant.now().isAfter(Instant.ofEpochMilli(killTime()));
    }

}
