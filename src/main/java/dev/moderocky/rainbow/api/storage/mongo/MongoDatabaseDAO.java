package dev.moderocky.rainbow.api.storage.mongo;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.internal.utility.FileManager;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.annotation.FutureAction;
import dev.moderocky.rainbow.api.annotation.Sync;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.StoredElement;
import dev.moderocky.rainbow.api.element.User;
import dev.moderocky.rainbow.api.element.implement.ProvisionalGroup;
import dev.moderocky.rainbow.api.element.implement.ProvisionalUser;
import dev.moderocky.rainbow.api.element.implement.RainbowGroup;
import dev.moderocky.rainbow.api.element.implement.RainbowUser;
import dev.moderocky.rainbow.api.storage.CacheDAO;
import dev.moderocky.rainbow.api.storage.Compactor;
import dev.moderocky.rainbow.api.storage.DAO;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class MongoDatabaseDAO implements DAO {

    final ExecutorService threadPool = Rainbow.THREAD_POOL;
    final Compactor compactor = Rainbow.COMPACTOR;
    final CacheDAO cacheDAO = new CachedMongoDAO();
    final DAO dao = this;
    final File groupFolder = new File("plugins/RainbowCommand/data/groups/");
    final File userFolder = new File("plugins/RainbowCommand/data/users/");
    final File bundleFolder = new File("plugins/RainbowCommand/data/bundles/");
    final List<StoredElement> savingQueue = new ArrayList<>();
    final ScheduledFuture<?> autosave;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public MongoDatabaseDAO() {
        groupFolder.mkdirs();
        userFolder.mkdirs();
        bundleFolder.mkdirs();
        final Runnable processQueue = () -> {
            for (StoredElement element : savingQueue) {
                element.save();
            }
            savingQueue.clear();
        };
        autosave = Rainbow.SCHEDULER.scheduleAtFixedRate(processQueue, 120, 120, TimeUnit.SECONDS);
    }

    @Override
    public void shutdown() {
        autosave.cancel(true);
        for (StoredElement element : savingQueue) {
            element.save();
        }
        savingQueue.clear();
    }

    @Override
    public boolean userExists(@NotNull UUID uuid) {
        File file = getFile(uuid);
        return file.exists() && file.isFile();
    }

    @Override
    public boolean groupExists(@NotNull String id) {
        File file = getFile(id);
        return file.exists() && file.isFile();
    }

    @Override
    public User getUser(@NotNull UUID uuid) {
        if (!userExists(uuid)) return null;
        if (cacheDAO.hasUser(uuid)) return cacheDAO.getUser(uuid);
        JsonObject object = getContent(getFile(uuid));
        return cache(new RainbowUser(object));
    }

    @Override
    public @NotNull User getOrCreateUser(@NotNull UUID uuid) {
        if (userExists(uuid)) return getUser(uuid);
        return cache(new RainbowUser(uuid));
    }

    @Override
    public Group getGroup(@NotNull String id) {
        if (!groupExists(id)) return null;
        if (cacheDAO.hasGroup(id)) return cacheDAO.getGroup(id);
        JsonObject object = getContent(getFile(id));
        return cache(new RainbowGroup(object));
    }

    @Override
    public @NotNull Group getOrCreateGroup(@NotNull String id) {
        if (groupExists(id)) return getGroup(id);
        return cache(new RainbowGroup(id));
    }

    @Override
    public @NotNull List<Group> getGroups() {
        final List<Group> groups = new ArrayList<>();
        File[] files = groupFolder.listFiles();
        if (files == null || files.length == 0) return groups;
        for (File file : files) {
            String[] parts = file.getName().split("/");
            String id = parts[parts.length - 1].replace(".json", "");
            groups.add(getCache().getFunctionalGroup(id));
        }
        return groups;
    }

    @Override
    public @NotNull List<User> getUsers() {
        final List<User> users = new ArrayList<>();
        File[] files = groupFolder.listFiles();
        if (files == null || files.length == 0) return users;
        for (File file : files) {
            String[] parts = file.getName().split("/");
            String id = parts[parts.length - 1].replace(".json", "");
            users.add(getCache().getFunctionalUser(UUID.fromString(id)));
        }
        return users;
    }

    @Override
    public void save(User user) {
        File file = getFile(user);
        write(file, user.getAsJson());
    }

    @Override
    public void save(Group group) {
        File file = getFile(group);
        write(file, group.getAsJson());
    }

    @Override
    public void getUserAsync(@NotNull UUID uuid, @NotNull Consumer<User> completable) {
        async(() -> completable.accept(getUser(uuid)));
    }

    @Override
    public void getGroupAsync(@NotNull String id, @NotNull Consumer<Group> completable) {
        async(() -> completable.accept(getGroup(id)));
    }

    @Override
    public void getUsersAsync(@NotNull Consumer<List<User>> completable) {
        async(() -> completable.accept(getUsers()));
    }

    @Override
    public void getGroupsAsync(@NotNull Consumer<List<Group>> completable) {
        async(() -> completable.accept(getGroups()));
    }

    @Override
    public void scheduleSave(StoredElement element) {
        if (!savingQueue.contains(element)) savingQueue.add(element);
    }

    @Override
    public CacheDAO getCache() {
        return cacheDAO;
    }

    @Override
    public @NotNull DAO getSource() {
        return this;
    }

    private File getFile(UUID uuid) {
        return new File(userFolder, uuid + ".json");
    }

    private File getFile(User user) {
        return getFile(user.getUUID());
    }

    private File getFile(String groupID) {
        return new File(groupFolder, groupID + ".json");
    }

    private File getFile(Group group) {
        return getFile(group.getID());
    }

    private File createFile(String path) {
        File file = new File(path);
        FileManager.putIfAbsent(file);
        return file;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void delete(File file) {
        file.delete();
    }

    private void create(File file) {
        FileManager.putIfAbsent(file);
    }

    private void clear(File file) {
        FileManager.clear(file);
    }

    private JsonObject getContent(File file) {
        final String string = FileManager.getContent(file);
        byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        if (compactor.isZipped(bytes)) return (JsonObject) Rainbow.JSON_PARSER.parse(compactor.unzip(bytes));
        return (JsonObject) Rainbow.JSON_PARSER.parse(string);
    }

    private void write(File file, JsonElement element) {
        byte[] bytes = compactor.zip(element.toString());
        FileManager.write(file, new String(bytes));
    }

    public void async(Runnable runnable) {
        threadPool.submit(runnable);
    }

    protected class CachedMongoDAO implements CacheDAO {

        final Map<UUID, User> userMap = new HashMap<>();
        final Map<String, Group> groupMap = new HashMap<>();

        @Override
        @Sync
        @FutureAction
        public ProvisionalUser getProvisionalUser(UUID uuid) {
            ProvisionalUser user = new ProvisionalUser();
            user.uuid = uuid;
            async(() -> {
                User real = getSource().getOrCreateUser(uuid);
                user.name = real.getName();
                user.joined = real.hasJoined();
                user.primaryGroup = real.getPrimaryGroup();
            });
            return user;
        }

        @Override
        @Sync
        @FutureAction
        public ProvisionalGroup getProvisionalGroup(String id) {
            ProvisionalGroup group = new ProvisionalGroup();
            group.id = id;
            async(() -> {
                Group real = getSource().getOrCreateGroup(id);
                group.name = real.getDisplayName();
                group.members = real.hasMembers();
                group.def = real.isDefault();
            });
            return group;
        }

        @Override
        public boolean hasUser(@NotNull UUID uuid) {
            return userMap.containsKey(uuid);
        }

        @Override
        public boolean hasGroup(@NotNull String id) {
            return groupMap.containsKey(id);
        }

        @Override
        public void cacheUser(@NotNull User user) {
            userMap.put(user.getUUID(), user);
        }

        @Override
        public void removeCache(@NotNull User user) {
            userMap.remove(user.getUUID());
        }

        @Override
        public void clearUserCache() {
            userMap.clear();
        }

        @Override
        public void cacheGroup(@NotNull Group group) {
            groupMap.put(group.getID(), group);
        }

        @Override
        public void removeCache(@NotNull Group group) {
            groupMap.remove(group.getID());
        }

        @Override
        public void clearGroupCache() {
            groupMap.clear();
        }

        @Override
        public User getUser(@NotNull UUID uuid) {
            return userMap.get(uuid);
        }

        @Override
        public Group getGroup(@NotNull String id) {
            return groupMap.get(id);
        }

        @Override
        public @NotNull List<Group> getGroups() {
            return new ArrayList<>(groupMap.values());
        }

        @Override
        public @NotNull List<User> getUsers() {
            return new ArrayList<>(userMap.values());
        }

        @Override
        public @NotNull DAO getSource() {
            return dao;
        }
    }
}
