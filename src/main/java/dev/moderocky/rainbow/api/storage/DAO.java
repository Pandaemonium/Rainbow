package dev.moderocky.rainbow.api.storage;

import dev.moderocky.rainbow.api.annotation.Async;
import dev.moderocky.rainbow.api.annotation.BlockingAction;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.StoredElement;
import dev.moderocky.rainbow.api.element.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public interface DAO {

    @BlockingAction
    boolean userExists(@NotNull UUID uuid);

    @BlockingAction
    boolean groupExists(@NotNull String id);

    @BlockingAction
    User getUser(@NotNull UUID uuid);

    @BlockingAction
    @NotNull User getOrCreateUser(@NotNull UUID uuid);

    @BlockingAction
    Group getGroup(@NotNull String id);

    @BlockingAction
    @NotNull Group getOrCreateGroup(@NotNull String id);

    @BlockingAction
    @NotNull List<Group> getGroups();

    @BlockingAction
    @NotNull List<User> getUsers();

    void save(User user);

    void save(Group group);

    @Async
    void getUserAsync(@NotNull UUID uuid, @NotNull Consumer<User> completable);

    @Async
    void getGroupAsync(@NotNull String id, @NotNull Consumer<Group> completable);

    @Async
    void getUsersAsync(@NotNull Consumer<List<User>> completable);

    @Async
    void getGroupsAsync(@NotNull Consumer<List<Group>> completable);

    default User cache(User user) {
        getCache().cacheUser(user);
        return user;
    }

    default Group cache(Group group) {
        getCache().cacheGroup(group);
        return group;
    }

    void async(Runnable runnable);

    void scheduleSave(StoredElement element);

    CacheDAO getCache();

    default @NotNull DAO getSource() {
        return this;
    }

    default void shutdown() {
    }

}
