package dev.moderocky.rainbow.api.storage;

import com.google.gson.JsonObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Compactor {

    public byte[] createEmptyJson() {
        return zip(new JsonObject().toString());
    }

    public byte[] zip(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream)) {
                gzipOutputStream.write(str.getBytes(StandardCharsets.UTF_8));
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException iOException) {

            return null;
        }
    }

    public String unzip(byte[] compressed) {
        if (compressed == null || compressed.length == 0) {
            return null;
        }
        if (!isZipped(compressed)) {
            return new String(compressed);
        }

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(compressed);
             GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
             InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            StringBuilder output = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                output.append(line);
            }
            return output.toString();


        } catch (IOException iOException) {

            return null;
        }
    }

    public boolean isZipped(byte[] compressed) {
        return (compressed[0] == 31 && compressed[1] == -117);
    }

}
