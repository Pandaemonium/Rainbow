package dev.moderocky.rainbow.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used for return elements that have the potential to be nullable,
 * but have a specific boolean method to pre-check them.
 * <p>
 * This is used to avoid pesky IDE/strict compiler warnings
 * about ignoring nullable potential when, in fact, it is assured.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Conditionull {
}
