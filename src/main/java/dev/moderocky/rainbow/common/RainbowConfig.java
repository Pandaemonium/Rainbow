package dev.moderocky.rainbow.common;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.template.Config;
import org.jetbrains.annotations.NotNull;

public class RainbowConfig implements Config {

    @Configurable(section = "storage")
    public String storageType = "file";

    public RainbowConfig() {
        load();
    }

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/RainbowCommand/";
    }

    @Override
    public @NotNull String getFileName() {
        return "config.yml";
    }

}
