package dev.moderocky.rainbow.backend.logic;

import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.backend.Rainbukkit;
import org.bukkit.Bukkit;
import org.bukkit.permissions.*;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class RainbowPermBase extends PermissibleBase implements dev.moderocky.rainbow.api.element.Permissible {
    private final ServerOperator opable;
    private final List<PermissionAttachment> attachments;
    private final Map<String, PermissionAttachmentInfo> permissions;
    private final PermissionAttachment rainbow;
    private Permissible parent = this;

    public RainbowPermBase(@Nullable ServerOperator opable) {
        super(opable);
        attachments = new LinkedList<>();
        permissions = new HashMap<>();
        this.opable = opable;

        if (opable instanceof Permissible) {
            this.parent = (Permissible) opable;
        }

        recalculatePermissions();
        rainbow = addAttachment(Rainbukkit.getInstance());
    }

    public RainbowPermBase(@Nullable ServerOperator opable, PermissibleBase base) {
        super(opable);
        this.opable = opable;
        attachments = getAttachments(base);
        permissions = getPermissionInfo(base);

        if (opable instanceof Permissible) {
            this.parent = (Permissible) opable;
        }

        recalculatePermissions();
        rainbow = addAttachment(Rainbukkit.getInstance());
    }

    private static List<PermissionAttachment> getAttachments(PermissibleBase base) {
        try {
            Field field = base.getClass().getDeclaredField("attachments");
            field.setAccessible(true);
            return new LinkedList<>((List<PermissionAttachment>) field.get(base));
        } catch (Throwable throwable) {
            return new LinkedList<>();
        }
    }

    private static Map<String, PermissionAttachmentInfo> getPermissionInfo(PermissibleBase base) {
        try {
            Field field = base.getClass().getDeclaredField("permissions");
            field.setAccessible(true);
            return new HashMap<>((Map<String, PermissionAttachmentInfo>) field.get(base));
        } catch (Throwable throwable) {
            return new HashMap<>();
        }
    }

    public boolean isOp() {
        if (opable == null) {
            return false;
        } else {
            return opable.isOp();
        }
    }

    public void setOp(boolean value) {
        if (opable == null) {
            throw new UnsupportedOperationException("Cannot change op value as no ServerOperator is set");
        } else {
            opable.setOp(value);
        }
    }

    public boolean isPermissionSet(@NotNull String name) {
        for (String string : permissions.keySet()) {
            if (Rainbow.CHECKER.matches(string, name)) return true;
        }
        return permissions.containsKey(name.toLowerCase());
    }

    private boolean getPermissionValue(@NotNull String name) {
        for (String string : permissions.keySet()) {
            if (Rainbow.CHECKER.matches(string, name)) return permissions.get(string).getValue();
        }
        return permissions.get(name.toLowerCase()).getValue();
    }

    @Override
    public List<String> getPermissions() {
        return new ArrayList<>(permissions.keySet());
    }

    @Override
    public boolean hasFinalPermission(String permission) {
        return hasPermission(permission);
    }

    @Override
    public boolean isFinalPermissionSet(String permission) {
        return isPermissionSet(permission);
    }

    @Override
    public @Unmodifiable Collection<String> getFinalPermissions() {
        return permissions.values().stream().map(PermissionAttachmentInfo::getPermission).collect(Collectors.toList());
    }

    @Override
    public HashMap<String, Boolean> getEffectiveFinalPermissions() {
        HashMap<String, Boolean> map = new HashMap<>();
        for (String permission : getPermissions()) {
            if (permission.startsWith("-")) map.put(permission.substring(1), false);
            else map.put(permission, true);
        }
        return map;
    }

    public boolean isPermissionSet(@NotNull Permission perm) {
        return isPermissionSet(perm.getName());
    }

    public @NotNull Set<PermissionAttachmentInfo> getEffectivePermissions() {
        return new HashSet<>(permissions.values());
    }

    @Override
    public void setPermission(String permission, boolean value) {
        rainbow.setPermission(permission, value);
    }

    @Override
    public void removePermission(String permission) {
        rainbow.unsetPermission(permission);
    }

    public boolean hasPermission(@NotNull String inName) {

        String name = inName.toLowerCase();

        if (isPermissionSet(name)) {
            return getPermissionValue(name);
        } else {
            Permission perm = Bukkit.getServer().getPluginManager().getPermission(name);
            if (perm != null) {
                return perm.getDefault().getValue(isOp());
            } else {
                return Permission.DEFAULT_PERMISSION.getValue(isOp());
            }
        }
    }

    public boolean hasPermission(@NotNull Permission perm) {

        String name = perm.getName().toLowerCase();

        if (isPermissionSet(name)) {
            return permissions.get(name).getValue();
        }
        return perm.getDefault().getValue(isOp());
    }

    public @NotNull PermissionAttachment addAttachment(@NotNull Plugin plugin, @NotNull String name, boolean value) {
        if (!plugin.isEnabled()) {
            throw new IllegalArgumentException("Plugin " + plugin.getDescription().getFullName() + " is disabled");
        }

        PermissionAttachment result = addAttachment(plugin);
        result.setPermission(name, value);

        recalculatePermissions();

        return result;
    }

    public @NotNull PermissionAttachment addAttachment(@NotNull Plugin plugin) {
        if (!plugin.isEnabled()) {
            throw new IllegalArgumentException("Plugin " + plugin.getDescription().getFullName() + " is disabled");
        }

        PermissionAttachment result = new PermissionAttachment(plugin, parent);

        attachments.add(result);
        recalculatePermissions();

        return result;
    }

    public void removeAttachment(@NotNull PermissionAttachment attachment) {

        if (attachments.contains(attachment)) {
            attachments.remove(attachment);
            PermissionRemovedExecutor ex = attachment.getRemovalCallback();

            if (ex != null) {
                ex.attachmentRemoved(attachment);
            }

            recalculatePermissions();
        } else {
            throw new IllegalArgumentException("Given attachment is not part of Permissible object " + parent);
        }
    }

    public void recalculatePermissions() {
        clearPermissions();
        Set<Permission> defaults = Bukkit.getServer().getPluginManager().getDefaultPermissions(isOp());
        Bukkit.getServer().getPluginManager().subscribeToDefaultPerms(isOp(), parent);

        for (Permission perm : defaults) {
            String name = perm.getName().toLowerCase();
            permissions.put(name, new PermissionAttachmentInfo(parent, name, null, true));
            Bukkit.getServer().getPluginManager().subscribeToPermission(name, parent);
            calculateChildPermissions(perm.getChildren(), false, null);
        }

        for (PermissionAttachment attachment : attachments) {
            calculateChildPermissions(attachment.getPermissions(), false, attachment);
        }
    }

    public synchronized void clearPermissions() {
        Set<String> perms = permissions.keySet();

        for (String name : perms) {
            Bukkit.getServer().getPluginManager().unsubscribeFromPermission(name, parent);
        }

        Bukkit.getServer().getPluginManager().unsubscribeFromDefaultPerms(false, parent);
        Bukkit.getServer().getPluginManager().unsubscribeFromDefaultPerms(true, parent);

        permissions.clear();
    }

    private void calculateChildPermissions(Map<String, Boolean> children, boolean invert, PermissionAttachment attachment) {
        Set<String> keys = children.keySet();

        for (String name : keys) {
            Permission perm = Bukkit.getServer().getPluginManager().getPermission(name);
            boolean value = children.get(name) ^ invert;
            String lname = name.toLowerCase();

            permissions.put(lname, new PermissionAttachmentInfo(parent, lname, attachment, value));
            Bukkit.getServer().getPluginManager().subscribeToPermission(name, parent);

            if (perm != null) {
                calculateChildPermissions(perm.getChildren(), !value, attachment);
            }
        }
    }

    public PermissionAttachment addAttachment(@NotNull Plugin plugin, @NotNull String name, boolean value, int ticks) {
        if (!plugin.isEnabled()) {
            throw new IllegalArgumentException("Plugin " + plugin.getDescription().getFullName() + " is disabled");
        }

        PermissionAttachment result = addAttachment(plugin, ticks);

        if (result != null) {
            result.setPermission(name, value);
        }

        return result;
    }

    public PermissionAttachment addAttachment(@NotNull Plugin plugin, int ticks) {
        if (!plugin.isEnabled()) {
            throw new IllegalArgumentException("Plugin " + plugin.getDescription().getFullName() + " is disabled");
        }

        PermissionAttachment result = addAttachment(plugin);

        if (Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new RemoveAttachmentRunnable(result), ticks) == -1) {
            Bukkit.getServer().getLogger().log(Level.WARNING, "Could not add PermissionAttachment to " + parent + " for plugin " + plugin.getDescription().getFullName() + ": Scheduler returned -1");
            result.remove();
            return null;
        } else {
            return result;
        }
    }

    public final PermissionAttachment getRainbow() {
        return rainbow;
    }

    private static class RemoveAttachmentRunnable implements Runnable {
        private final PermissionAttachment attachment;

        public RemoveAttachmentRunnable(PermissionAttachment attachment) {
            this.attachment = attachment;
        }

        public void run() {
            attachment.remove();
        }
    }
}
