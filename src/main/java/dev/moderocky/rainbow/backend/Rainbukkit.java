package dev.moderocky.rainbow.backend;

import com.moderocky.mask.template.BukkitPlugin;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.internal.CoreConstant;
import dev.moderocky.rainbow.backend.logic.RainbowPermBase;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Level;

public class Rainbukkit extends BukkitPlugin implements CoreConstant {

    public static final String CRAFT_VERSION;
    public static final Field PERM_FIELD;

    static {
        try {
            CRAFT_VERSION = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            PERM_FIELD = Class.forName("org.bukkit.craftbukkit." + CRAFT_VERSION + ".entity.CraftHumanEntity").getDeclaredField("perm");
            PERM_FIELD.setAccessible(true);
            Field modifiers = PERM_FIELD.getClass().getDeclaredField("modifiers");
            modifiers.setAccessible(true);
            modifiers.setInt(modifiers, PERM_FIELD.getModifiers() & ~Modifier.FINAL);
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            Bukkit.getLogger().log(Level.SEVERE, "An error occurred while enabling this plugin.");
            throw new RuntimeException(e);
        }
    }

    public Rainbow rainbow;

    public static void applyConversion(Player player) {
        try {
            PermissibleBase base = (PermissibleBase) PERM_FIELD.get(player);
            PERM_FIELD.set(player, new RainbowPermBase(player, base));
        } catch (Throwable ignore) {

        }
    }

    @Override
    public void startup() {
        rainbow = new Rainbow(this);
    }

    @Override
    public void disable() {
        rainbow.shutdown();
        rainbow = null;
    }

}
