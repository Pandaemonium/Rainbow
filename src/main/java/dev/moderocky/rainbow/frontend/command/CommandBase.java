package dev.moderocky.rainbow.frontend.command;

import com.moderocky.mask.command.Commander;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.Group;
import dev.moderocky.rainbow.api.element.MetaComponent;
import dev.moderocky.rainbow.api.element.VisualElement;
import dev.moderocky.rainbow.api.element.Weighted;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public enum CommandBase {

    PLAYER {
        @Override
        Consumer<Player> help(Commander<?> commander) {
            return null;
        }

        @Override
        Consumer<Player> listGroups() {
            return null;
        }

        @Override
        BiConsumer groupInfo() {
            return null;
        }

        @Override
        String getCommand() {
            return "raingui";
        }
    },
    BUKKIT {
        @Override
        Consumer<org.bukkit.command.CommandSender> help(Commander<?> commander) {
            return sender -> {
                ComponentBuilder builder = new ComponentBuilder("")
                        .color(ChatColor.WHITE).append(Rainbow.PREFIX).append("Command Help:");
                for (String pattern : commander.getPatterns()) {
                    builder
                            .append(System.lineSeparator())
                            .reset()
                            .append(" - /" + commander.getCommand())
                            .color(ChatColor.DARK_GRAY)
                            .append(" ")
                            .append(pattern)
                            .event(new ClickEvent((pattern.contains("[") || pattern.contains("<")) ? ClickEvent.Action.SUGGEST_COMMAND : ClickEvent.Action.RUN_COMMAND, "/" + commander.getCommand() + " " + pattern.replaceFirst("(<.*|\\[.*)", "")))
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText((pattern.contains("[") || pattern.contains("<")) ? "Click to suggest." : "Click to run.", ChatColor.AQUA)))
                            .color(ChatColor.GRAY);
                }
                sender.sendMessage(builder.create());
            };
        }

        @Override
        Consumer<org.bukkit.command.CommandSender> listGroups() {
            return sender -> {
                ComponentBuilder builder = new ComponentBuilder("")
                        .color(ChatColor.WHITE).append(Rainbow.PREFIX).append("List of Groups:");
                for (Group group : Rainbow.DAO.getGroups()) {
                    builder
                            .append(System.lineSeparator())
                            .reset()
                            .append(" - ").color(ChatColor.DARK_GRAY)
                            .append(group.getEffectiveName())
                            .color(ChatColor.WHITE)
                            .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/" + getCommand() + " group " + group.getID()))
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("").color(ChatColor.GRAY)
                                    .append("ID: ").color(ChatColor.GRAY).append(group.getID()).color(ChatColor.WHITE).append(System.lineSeparator()).reset()
                                    .append("Prefix: ").color(ChatColor.GRAY).append(group.hasPrefix() ? group.getLegacyPrefix() : "§fNone").append(System.lineSeparator()).reset()
                                    .append("Suffix: ").color(ChatColor.GRAY).append(group.hasSuffix() ? group.getLegacySuffix() : "§fNone").append(System.lineSeparator()).reset()
                                    .create()));
                }
                sender.sendMessage(builder.create());
            };
        }

        @Override
        BiConsumer<org.bukkit.command.CommandSender, Object[]> groupInfo() {
            return (sender, input) -> {
                Group group = (Group) input[0];
                ComponentBuilder builder = new ComponentBuilder("")
                        .color(ChatColor.WHITE).append(Rainbow.PREFIX).append("Group Information:");
                builder
                        .append(System.lineSeparator())
                        .reset()
                        .append("Name: ").color(ChatColor.GRAY).append(group.getEffectiveName()).color(ChatColor.WHITE).append(System.lineSeparator()).reset()
                        .append("ID: ").color(ChatColor.GRAY).append(group.getID()).color(ChatColor.WHITE).append(System.lineSeparator()).reset();
                if (group.hasPrefix()) {
                    builder
                            .append("Prefixes: ").color(ChatColor.GRAY);
                    boolean first = true;
                    for (MetaComponent prefix : Weighted.sort(group.getMetaEntries("prefix"))) {
                        builder.append(System.lineSeparator()).reset();
                        builder.append(" - ").color(first ? ChatColor.GOLD : ChatColor.DARK_GRAY);
                        builder.append(VisualElement.getComponent(prefix.getValue()));
                        if (first) first = false;
                    }
                } else {
                    builder.append("Prefix: ").color(ChatColor.GRAY).append("§fNone").append(System.lineSeparator()).reset();
                }
                if (group.hasSuffix()) {
                    builder
                            .append("Suffixes: ").color(ChatColor.GRAY);
                    boolean first = true;
                    for (MetaComponent suffix : Weighted.sort(group.getMetaEntries("suffix"))) {
                        builder.append(System.lineSeparator()).reset();
                        builder.append(" - ").color(first ? ChatColor.GOLD : ChatColor.DARK_GRAY);
                        builder.append(VisualElement.getComponent(suffix.getValue()));
                        if (first) first = false;
                    }
                } else {
                    builder.append("Suffix: ").color(ChatColor.GRAY).append("§fNone").append(System.lineSeparator()).reset();
                }

                sender.sendMessage(builder.create());
            };
        }

        @Override
        String getCommand() {
            return "rainbow";
        }
    },
    PROXY {
        @Override
        Consumer<net.md_5.bungee.api.CommandSender> help(Commander<?> commander) {
            return null;
        }

        @Override
        Consumer<net.md_5.bungee.api.CommandSender> listGroups() {
            return null;
        }

        @Override
        BiConsumer groupInfo() {
            return null;
        }

        @Override
        String getCommand() {
            return "rainproxy";
        }
    };

    CommandBase() {

    }

    abstract Consumer help(Commander<?> commander);

    abstract Consumer listGroups();

    abstract BiConsumer groupInfo();

    abstract String getCommand();


}
