package dev.moderocky.rainbow.frontend.command.argument;

import com.moderocky.mask.command.Argument;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.Group;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArgGroup implements Argument<Group> {

    private String label = "group";
    private boolean required = true;

    @Override
    public @NotNull Group serialise(String string) {
        return Rainbow.DAO.getGroup(string);
    }

    @Override
    public boolean matches(String string) {
        return Rainbow.DAO.groupExists(string);
    }

    @Override
    public @NotNull String getName() {
        return label;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        List<String> strings = new ArrayList<>();
        Rainbow.DAO.getGroups().forEach(group -> strings.add(group.getID()));
        return strings;
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgGroup setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgGroup setLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

}
