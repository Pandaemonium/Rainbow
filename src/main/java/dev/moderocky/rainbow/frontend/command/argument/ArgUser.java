package dev.moderocky.rainbow.frontend.command.argument;

import com.moderocky.mask.command.Argument;
import dev.moderocky.rainbow.api.Rainbow;
import dev.moderocky.rainbow.api.element.User;
import net.md_5.bungee.api.ProxyServer;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ArgUser implements Argument<User> {

    private String label = "name/uuid";
    private boolean required = true;

    @Override
    public @NotNull User serialise(String string) {
        try {
            UUID uuid = UUID.fromString(string);
            return Rainbow.DAO.getOrCreateUser(uuid);
        } catch (Throwable isName) { // If the user provided a name
            try {
                UUID uuid = Bukkit.getPlayerUniqueId(string);
                if (uuid == null) throw new IllegalArgumentException();
                return Rainbow.DAO.getOrCreateUser(uuid);
            } catch (Throwable noPaper) {
                try {
                    UUID uuid = Bukkit.getOfflinePlayer(string).getUniqueId();
                    return Rainbow.DAO.getOrCreateUser(uuid);
                } catch (Throwable notSeen) {
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    @Override
    public boolean matches(String string) {
        try {
            UUID uuid = UUID.fromString(string);
            return Rainbow.DAO.userExists(uuid);
        } catch (Throwable isName) {
            if (Rainbow.getInstance().getPlugin().isBukkit()) {
                try {
                    if (Bukkit.getPlayer(string) != null) {
                        UUID uuid = Bukkit.getPlayer(string).getUniqueId();
                        return Rainbow.DAO.userExists(uuid);
                    } else {
                        UUID uuid = Bukkit.getPlayerUniqueId(string);
                        if (uuid == null) return false;
                        return Rainbow.DAO.userExists(uuid);
                    }
                } catch (Throwable noPaper) {
                    try {
                        UUID uuid = Bukkit.getOfflinePlayer(string).getUniqueId();
                        return Rainbow.DAO.userExists(uuid);
                    } catch (Throwable notSeen) {
                        return false;
                    }
                }
            } else {
                try {
                    return ProxyServer.getInstance().getPlayer(string) != null;
                } catch (Throwable notSeen) {
                    return false;
                }
            }
        }
    }

    @Override
    public @NotNull String getName() {
        return label;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        List<String> strings = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player -> strings.add(player.getName()));
        return strings;
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgUser setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgUser setLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

}
