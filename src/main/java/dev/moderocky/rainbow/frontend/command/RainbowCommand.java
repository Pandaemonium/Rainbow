package dev.moderocky.rainbow.frontend.command;

import com.moderocky.mask.command.ArgString;
import com.moderocky.mask.command.Commander;
import com.moderocky.mask.template.WrappedCommand;
import dev.moderocky.rainbow.frontend.command.argument.ArgGroup;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class RainbowCommand extends Commander<CommandSender> implements WrappedCommand {

    @Override
    @SuppressWarnings("unchecked")
    public @NotNull Main create() {
        return command("rainbow")
                .arg("help", getDefault())
                .arg("groups", (Consumer<CommandSender>) CommandBase.BUKKIT.listGroups())
                .arg("group",
                        arg(CommandBase.BUKKIT.groupInfo(), new ArgGroup()),
                        arg(CommandBase.BUKKIT.groupInfo(), new ArgGroup(), new ArgString())
                )
                ;
    }

    @Override
    @SuppressWarnings("unchecked")
    public @NotNull Consumer<CommandSender> getDefault() {
        return (Consumer<CommandSender>) CommandBase.BUKKIT.help(this);
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("rb", "rain", "rbow", "rainbukkit");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand() + " help";
    }

    @Override
    public @NotNull String getDescription() {
        return "The main command for Rainbow.";
    }

    @Override
    public @Nullable String getPermission() {
        return "rainbow.command.rainbow";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return "You do not have access to this! If you believe this is an error, please contact an administrator.";
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return execute(commandSender, strings);
    }

}
